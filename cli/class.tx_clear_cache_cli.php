<?php
if (!defined('TYPO3_cliMode')) {
    die('You cannot run this script directly!');
}

/**
 * class to process actions via CLI
 *
 * @package eft
 * @subpackage typo3
 */
class tx_clear_cache_cli  {
    /**
     * definition of the extension name
     * @var string
     */
    protected $extKey = 'clear_cache';
    /**
     * @var string
     */
    protected $prefixId = 'tx_clear_cache_cli'; // Same as class name
    /**
     * Path to this script relative to the extension dir
     * @var string
     */
    protected $scriptRelPath = 'cli/class.tx_clear_cache_cli.php';

    /** @var \TYPO3\CMS\Core\Cache\CacheManager  */
    protected $cacheManager ;

    /** @var \TYPO3\CMS\Extbase\Service\CacheService  */
    protected $cacheService ;
    /**
     * constructor
     */
    public function __construct() {

        $this->cacheManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager');

        $pageIds = false ;
        $singleCache = false ;
        if($_SERVER[ 'argv' ][1]) {
            $singleCache = true ;
            if( $_SERVER[ 'argv' ][1] == "pages" ) {
                if($_SERVER[ 'argv' ][2]) {
                    $pageIds = explode( "," , $_SERVER[ 'argv' ][2] ) ;
                }
                if ( $pageIds  ) {
                    foreach ( $pageIds as $pageId ) {
                        if ( intval( $pageId ) > 0 ) {
                            $this->cacheManager->flushCachesInGroupByTag('pages', 'pageId_' . intval( $pageId)) ;
                            print("CACHE PID $pageId is CLEARED!\n");
                        }

                    }
                }
            }
        } else {
            $this->cacheManager->flushCaches();
            print("All CACHES CLEARED!\n");
            print("To Clear just one page use:  php typo3/cli_dispatch.phpsh clear_t3cache pages 12 \n");
            print("or add pageIds komma separated:  php typo3/cli_dispatch.phpsh clear_t3cache pages 12,123,125 \n");
        }
        if (function_exists('apc_clear_cache')) {
            apc_clear_cache("user");
            apc_clear_cache();
            print("APC  CACHES also CLEARED!\n");
        }


    }
}

$extensionkey = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('tx_clear_cache_cli');
