<?php

$EM_CONF['clear_t3cache'] = array (
	'title' => 'clear_t3cache',
	'description' => 'extension with cli-script which clears the TYPO3-cache',
	'category' => 'be',
	'version' => '0.9.5',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearcacheonload' => 0,
	'author' => 'Francisco J. López Sanhueza',
	'author_email' => 'fjlopezs87@gmail.com',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '6.1.0-7.6.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

